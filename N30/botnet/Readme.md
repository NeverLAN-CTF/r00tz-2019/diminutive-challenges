## Botnet

---

### Message

Someone is attempting to use my IoT devices as a botnet. Can you find out who their target is?

### Files

  - ssdp.pcap

### Flag

`flag{Y0v3r3_a_b0t_n0w}`
