## The Pcap

---

### Difficulty

Medium. 6/10.

### Message

There were a set of messages sent between a Tycho Ship & "The ENEMY" ship. 
You want to intercept the payment. There is a code needed to do so. That is the flag.

### Files

  - comms.cap

### Flag

`FredJohn`
